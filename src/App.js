import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './reponsive.css'
import './css/certificate.css'
import Header from "./component/header"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from './routes';





class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
       <Router>
            <Switch>
                { this.showContentMenu(routes) }
            </Switch>
           </Router>
      </div>
    );
  }


      showContentMenu = (routes) => {
        var result = null;

        if (routes.length > 0) {
            result = routes.map((route, index) => {
                return (
                    <Route 
                      key={index} 
                      path={route.path} 
                      exact={route.exact} 
                      component={route.main} 
                    />
                );
            });
        }

        return result;
    }




}

export default App;
