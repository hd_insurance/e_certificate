import React from 'react';
import Certificate from './component/certificate';
import Page404 from './component/page404.js';

const routes = [
   {
        path : '/',
        exact : true,
        main : Certificate
    },
    {
        path : '*',
        exact : true,
        main : Page404
    }
];

export default routes;