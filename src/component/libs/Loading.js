import React, { useEffect, useState } from "react";
import "./style.css"


const LoadingICO = (props) => {
  return (
      <div className="loading-ico">
        <div className="half-circle-spinner">
          <div className="circle circle-1"></div>
          <div className="circle circle-2"></div>
        </div>
      </div>

  );
};
export default LoadingICO;
