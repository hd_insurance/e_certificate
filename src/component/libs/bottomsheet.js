import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import useWindowDimensions from '../../hooks/useWindowDimensions';

import {
  motion,
  useMotionValue,
  useTransform,
  useAnimation,
} from "framer-motion"


import "./style.css"

const hasWindow = typeof window !== 'undefined';

function getWindowHeight() {
    const height = hasWindow ? window.innerHeight : null;
    return height - 200
  }


const BottomSheet = (props) => {
  const min = 80
  const max = getWindowHeight()
  const [isOpen, setOpen] = React.useState(false);
  const controls = useAnimation()




  controls.start({
    y: -max
  })



  const y = useMotionValue(0)
  const opacity = useTransform(
    y,
    [-max, min],
    [1, 0]
  )


  const onDragEnd = (py)=>{
    console.log(py)
    if(py < 300){
      setOpen(true)
      controls.start({
        y: -max
      })

    }else{
     setOpen(false)
     controls.start({
        y: min
      })
    }
  }
  const onDragStart = (py)=>{
   // setOpen(true)
  }

  const openSheet = (e)=>{
    setOpen(true)
    controls.start({
        y: - max
    })
  }

  const closeSheet = ()=>{
    // controls.start({
    //     y: max
    // })
  }


  return (

      <div>
        <div className="sheet-container mhsheet" >
          <motion.div className="sheet-header"
              drag="y"
              initial={true}
              style={{y}}
              dragConstraints={{ top: -max, bottom: min }}
              onDragEnd={
                (event, info) => onDragEnd(info.point.y)
              }
              onDragStart={
                (event, info) => onDragStart(info.point.y)
              }
          >

           <div className="bottom-sheet-actor"  onClick={(e)=>openSheet(e)}>
              <p><i className="fas fa-chevron-circle-up"></i> Hiển thị chức năng khác</p>
            </div>

          </motion.div>
        
          <motion.div
            className="sheet-content"
            style={{height: (getWindowHeight() - 62), y:y}}
            animate={controls}>
            {props.children}
          </motion.div>

        </div>

         <motion.div className="bottom-sheet-backdrop" style={{ opacity, display:(isOpen?"block":"none")}}></motion.div>
      </div>

  );
};
export default BottomSheet;
