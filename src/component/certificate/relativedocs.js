import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { Col, Button } from "react-bootstrap";

const CertificateDocument = (props) => {
  let files = [];
  if (!props.files || !props.files.otherFile || props.files.otherFile?.length == 0) {
    return null;
  }
  const _e = props.files.CERTIFICATE_NO;
  files = props.files.otherFile.filter(p => p.CERTIFICATE_NO === _e);
  return (
    <div className="cer-docs">
      <ul>
        {files.length ? files.map((doc, index) => {
          return (
            <li key={index}>
              <a target="_blank" href={`${doc.URL_FILE}`}>
                <div className="icon-group">
                  <i className="fas fa-file-alt"></i>
                  <i className="fas fa-file-download"></i>
                </div>
                <div className="docname">{doc.FILE_NAME}</div>
              </a>
            </li>
          );
        }) : null}
      </ul>
    </div>
  );
};
export default CertificateDocument;
