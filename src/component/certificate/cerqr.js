import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import Language from "../../services/Language.js";
import QRCode from "qrcode.react";
import { StageSpinner } from "react-spinners-kit";

const l = new Language();

const CertificateQR = (props) => {
  const { certificate } = props;

  const [urlValue, setUrlValue] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (certificate) {
      let url = certificate.URL_CERTIFICATE;
      if (!certificate?.IS_WAIT) {
        setLoading(false);
        setUrlValue(url);
        return;
      }
      if (certificate?.IS_WAIT !== "TRUE") {
        setLoading(false);
        setUrlValue(url);
        return;
      }
      setLoading(true);
      if (certificate?.IS_WAIT === "TRUE") {
        url = `${url}${certificate.FILE_SIGNED}`;
        setTimeout(() => {
          setUrlValue(url);
          setLoading(false);
        }, 2000);
      }
    }
  }, [certificate]);

  const download = () => {
    const canvas = document.getElementById("qrcode-kitin");
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = "QR-Certificate.png";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };
  return (
    <div className="cer-qr">
      {loading ? (
        <div className="style_loading_cert">
         <StageSpinner size={50} loading={loading} color={"#329945"} />
       </div>
      ) : (
        <>
          <div className="img-qr">
            <QRCode id="qrcode-kitin" value={urlValue ? urlValue : ''} />
          </div>
          <div className="div-save-qr">
            <Button className="btn-save-qr" onClick={(e) => download()}>
              Lưu mã QR để tra cứu GCN lần sau
            </Button>
          </div>
        </>
      )}
    </div>
  );
};
export default CertificateQR;
