import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";
import queryString from "query-string";
import { Row, Modal } from "react-bootstrap";
import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile,
} from "react-device-detect";
import Sheet from "react-modal-sheet";
import Network from "../../services/Network";
import CerContent from "./gcncontent";
import LeftSide from "./leftsidebar";
import CerResult from "./certificateresult";

const api = new Network();

const Certificate = (props) => {
  const [isOpen, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [notfound, setNotfound] = useState(false);
  const [certificate, setCertificate] = useState(null);
  const [showModel, setShowModel] = useState(false);
  const [arrData, setArrData] = useState([]);
  const [value, setValue] = useState("");
  const [f, setF] = useState("");
  const [dataDefine, setDataDefine] = useState({
    dataType: [],
    dataTypeIsr: [],
  });

  const onTextChange = (e) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    let params = queryString.parse(props.location.search);
    if (params.id) {
      setF(params.id);
      search(null, params.id);
    }
    getDefineType();
  }, []);


  const handleData = (infoDetail = [], infoExt = [], otherFile = []) => {
    // console.log('infoDetail', infoDetail);
    // console.log('infoExt', infoExt);
    // console.log('otherFile', otherFile);
    try {
      const data = [];
      infoDetail.forEach(info => {
        let dataExt = [];
        infoExt.forEach(infoExtDetail => {
          if(infoExtDetail.DETAIL_CODE === info.DETAIL_CODE ) {
            dataExt.push({
              DESCRIPTION: infoExtDetail.DESCRIPTION,
              VALUE: infoExtDetail.VALUE,
            });
          }
        })
        let objData = {...info, infoExt: dataExt, otherFile: otherFile }
        data.push(objData);
      });
      // console.log('data', data);
      return data;
    } catch(err){
      console.log(err);
    }
  }

  const handleDefineData = (data) => {
    let result = JSON.parse(data.DESCRIPTION);
    let arrType  = [];
    let arrTypeIsr = [];
    arrType = result.map(item => {
      if(item.TYPE === 'FLY'){
        arrTypeIsr = item.PRODUCTS.map(typeIns => {
          return {
            label: typeIns.PRODUCT_NAME,
            value: typeIns.PRODUCT_CODE,
          }
        })
      }
      return {
        label: item.NAME,
        value: item.TYPE,
      }
    });
    return {dataType: [...arrType], dataTypeIsr: [...arrTypeIsr]}
  } 

  const getDefineType = async () => {
    try {
      const response = await api.post("/api/define-type");
      if(response.data.length > 0 && response.data?.[0]?.length > 0) {
        const data = handleDefineData(response.data[0][0]);
        setDataDefine(data);
      }
    } catch(err){
      console.log(err);
    }
  }

  const search = async (data, file = "") => {
    try {
      setF("");
      setLoading(true);
      setNotfound(false);
      const response = await api.post("/api/search-gcn", {...data, fileId: file});
      const dataResult = handleData(response.data[0], response.data[1], response.data[2]);
      setLoading(false);
      if(dataResult.length === 1){
        setCertificate(dataResult[0]);
        return;
      }
      if(dataResult.length > 1){
        setShowModel(true);
        setArrData(dataResult);
        return;
      }
      setNotfound(true);
      setCertificate(null);
    } catch (e) {
      setNotfound(true);
      setLoading(false);
      setCertificate(null);
    }
  };

  const onInputFocus = () => {
    console.log("onInputFocus");
    setNotfound(false);
  };
  const handleShowDetail = (data) => {
    setCertificate(data);
    setShowModel(false);
  };

  return (
    <div className="row-certificate">
      <Modal
        show={showModel}
        onHide={() => setShowModel(false)}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h5>Chọn Giấy Chứng Nhận</h5>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <Row>
              <p className="m-0">Chọn giấy chứng nhận bạn muốn xem</p>
            </Row>
            {arrData.map((item, index) => {
              return (
                <div key={index} className="cer-result-kitin">
                  <CerResult cerDetail={item ? item : {}} />
                  <div
                    className="btn result-kitin-detail"
                    onClick={() => handleShowDetail(item)}
                  >
                    Xem Chi Tiết
                  </div>
                </div>
              );
            })}
          </div>
        </Modal.Body>
        {/* <Modal.Footer>
          <Button onClick={() => setShowModel(false)}>Close</Button>
        </Modal.Footer> */}
      </Modal>

      <BrowserView>
        <LeftSide
          onInpFocus={onInputFocus}
          onTextChange={onTextChange}
          value={value}
          isnotfound={notfound}
          search={search}
          loading={loading}
          certificate={certificate}
          dataDefine={dataDefine}
        />
      </BrowserView>

      <MobileView>
        <Sheet isOpen={isOpen} onClose={() => setOpen(false)}>
          <Sheet.Container>
            <Sheet.Header />
            <Sheet.Content>
              <LeftSide
                onInpFocus={onInputFocus}
                isnotfound={notfound}
                search={search}
                loading={loading}
                certificate={certificate}
                dataDefine={dataDefine}
              />
            </Sheet.Content>
          </Sheet.Container>
          <Sheet.Backdrop />
        </Sheet>
      </MobileView>

      <div className="c-right-div">
        <CerContent
          onInpFocus={onInputFocus}
          onTextChange={onTextChange}
          value={value}
          isnotfound={notfound}
          search={search}
          loading={loading}
          onInpFocus={onInputFocus}
          certificate={certificate}
          dataDefine={dataDefine}
        />
      </div>

      <motion.div
        animate={{
          x: 0,
          y: certificate ? 0 : 51,
          scale: 1,
          rotate: 0,
        }}
        className="bottom-sheet-actor"
        onClick={(e) => setOpen(true)}
      >
        <p>
          <i className="fas fa-chevron-circle-up"></i> Hiển thị chức năng khác
        </p>
      </motion.div>
    </div>
  );
};
export default Certificate;
