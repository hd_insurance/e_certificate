import React, { useEffect, useState } from "react";
import { Col, Button, Form } from "react-bootstrap";
import LoadingIco from "../libs/Loading";

const Search = (props) => {
  const { url } = props;
  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() === false) {
      return setValidated(true);
    }

    props.search(props.value);
  };

  return (
    <div className="search-ctn-frame">
      <div>
        <Form
          noValidate
          validated={validated}
          className="search-input"
          onSubmit={handleSubmit}
        >
          <Form.Control
            type="text"
            value={props.value}
            onChange={props.onTextChange}
            placeholder={"Tìm kiếm theo GCN"}
            onFocus={() => {
              props.onInpFocus();
            }}
            required={true}
          />

          <Button className="ico" type="submit">
            {props.loading ? <LoadingIco /> : <i className="fas fa-search"></i>}
          </Button>
        </Form>
      </div>
      <div className={props.certificate ? "mh-hide-div" : ""}>
        <div className="search-default-frame">
          <img
            src={
              props.isnotfound
                ? "/img/sdk/notfound.svg"
                : "/img/sdk/search-icon-big.svg"
            }
          />
          <p>
            {props.isnotfound
              ? "Không tìm thấy Giấy chứng nhận. Vui lòng nhập tìm kiếm Giấy chứng nhận bằng cách nhập trên thanh tìm kiếm"
              : "Vui lòng tìm kiếm Giấy chứng nhận bằng cách nhập trên thanh tìm kiếm để xem Giấy chứng nhận"}
          </p>
        </div>
      </div>
    </div>
  );
};
export default Search;
