import React, { useState } from "react";
import { Form } from "react-bootstrap";
import FLInput from "../libs/uikit/flinput";
import FLSelect from "../libs/uikit/flselect";
import FLDate from "../libs/uikit/fldate";
import { MetroSpinner } from "react-spinners-kit";
import moment from "moment";

const AdvancedSearch = (props) => {
  const { url, dataDefine } = props;
  const [validated, setValidated] = useState(false);

  const [typeInsurance, setTypeInsurance] = useState("NHA_TU_NHAN");
  const [infoKYC, setInfoKYC] = useState("");
  const [fullname, setFullname] = useState("");
  const [dob, setDob] = useState(null);
  const [licensePlate, setLicensePlate] = useState("");
  const [phoneEmail, setPhoneEmail] = useState("");
  const [typeIsrFlight, setTypeIsrFlight] = useState("");
  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [fromDate, setFromDate] = useState(null);
  const [returnDate, setReturnDate] = useState(null);
  const [maDC, setMaDC] = useState("");

  const handleTypeInsurance = (typeIsr) => {
    if(typeIsr === 'FLY') {
      setTypeInsurance(typeIsr);
      setTypeIsrFlight("BAY_AT");
      return;
    }
    setTypeInsurance(typeIsr);
  }

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() === false) {
      return setValidated(true);
    }
    let data;
    switch(typeInsurance){
      case 'NHA_TU_NHAN': {
        data = {
          a2: infoKYC.trim(),
          a3: typeInsurance,
          a4: fullname.trim(),
        }
        break;
      }
      case 'CON_NGUOI': {
        data = {
          a2: infoKYC.trim(),
          a3: typeInsurance,
          a4: fullname.trim(),
          a5: dob,
          // ngày sinh
        }
        break;
      }
      case 'XE_COGIOI': {
        data = {
          a2: phoneEmail.trim(),
          a3: typeInsurance,
          a4: fullname.trim(),
          a7: licensePlate.trim(), // khung, biển số xe
        }
        break;
      }
      case 'FLY': {
        if(typeIsrFlight === "BAY_AT") {
          data = {
            a3: typeIsrFlight,
            a4: `${lastName.trim()} ${firstName.trim()}`,
            a5: fromDate,
            a7: maDC.trim(),
          }
          break;
        }
        if(typeIsrFlight === "DELAY") {
          data = {
            a2: phoneEmail.trim(),
            a3: typeIsrFlight,
            a4: `${lastName.trim()} ${firstName.trim()}`,
            a7: maDC.trim(),
          }
          break;
        }
        if(typeIsrFlight === "LOST_BAGGAGE") {
          data = {
            a2: phoneEmail.trim(),
            a3: typeIsrFlight,
            a4: `${lastName.trim()} ${firstName.trim()}`,
            a7: maDC.trim(),
          }
          break;
        }
        if(typeIsrFlight === "TRAVEL") {
          data = {
            a2: infoKYC.trim(),
            a3: typeIsrFlight,
            a4: fullname.trim(),
            a5: fromDate,
            a6: returnDate,
          }
          break;
        }
        break;
      }
    }
    props.search(data);
  };

  const maximumDate = () => {
    const now = moment();
    return {
      year: now.year(),
      month: now.month() + 1,
      day: now.date(),
    };
  };

  return (
    <div>
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-6 mt-3">
            <FLSelect
              disable={false}
              label={"Chọn loại bảo hiểm"}
              value={typeInsurance}
              changeEvent={handleTypeInsurance}
              dropdown={true}
              required={true}
              data={dataDefine.dataType}
            />
          </div>

          {["NHA_TU_NHAN", "CON_NGUOI"].includes(typeInsurance) ? (
            <div className="col-6 mt-3">
              <FLInput
                label="CMND/ CCCD/ HC/ SĐT"
                required={true}
                value={infoKYC}
                changeEvent={setInfoKYC}
              />
            </div>
          ) : null}

          {["NHA_TU_NHAN"].includes(typeInsurance) ? (
            <div className="col-6 mt-3">
              <FLInput
                label="Họ Tên Khách Hàng"
                required={true}
                value={fullname}
                changeEvent={setFullname}
              />
            </div>
          ) : null}

          {typeInsurance === "CON_NGUOI" ? (
            <>
              <div className="col-6 mt-3">
                <FLInput
                  label="Họ Tên"
                  required={true}
                  value={fullname}
                  changeEvent={setFullname}
                />
              </div>
              <div className="col-6 mt-3">
                <FLDate
                  disable={false}
                  value={dob}
                  changeEvent={setDob}
                  label={"Ngày Sinh"}
                  required={true}
                  maximumDate={maximumDate()}
                />
              </div>
            </>
          ) : null}

          {typeInsurance === "XE_COGIOI" ? (
            <>
              <div className="col-6 mt-3">
                <FLInput
                  label="Biển số/ Số khung/ máy"
                  required={true}
                  value={licensePlate}
                  changeEvent={setLicensePlate}
                />
              </div>
              <div className="col-6 mt-3">
                <FLInput
                  label="Họ Tên Chủ Xe"
                  required={true}
                  value={fullname}
                  changeEvent={setFullname}
                />
              </div>
              <div className="col-6 mt-3">
                <FLInput
                  label="SĐT/ Email"
                  required={true}
                  value={phoneEmail}
                  changeEvent={setPhoneEmail}
                />
              </div>
            </>
          ) : null}

          {typeInsurance === "FLY" ? (
            <>
              <div className="col-6 mt-3">
                <FLSelect
                  disable={false}
                  label={"Bảo hiểm"}
                  value={typeIsrFlight}
                  changeEvent={setTypeIsrFlight}
                  dropdown={true}
                  required={true}
                  data={dataDefine.dataTypeIsr}
                />
              </div>
              {["BAY_AT", "DELAY", "LOST_BAGGAGE"].includes(typeIsrFlight) ? (
                <>
                  <div className="col-12 mt-3">
                    <div className="group-ipnv">
                      <FLInput
                        disable={false}
                        value={lastName}
                        changeEvent={(v) => setLastName(v)}
                        label={"Họ"}
                        hideborder={true}
                        required={true}
                      />
                      <div className="ver-line"></div>
                      <div className="kkjs">
                        <FLInput
                          disable={false}
                          value={firstName}
                          changeEvent={(v) => setFirstName(v)}
                          hideborder={true}
                          label={"Tên"}
                          required={true}
                        />
                      </div>
                    </div>
                  </div>
                  {typeIsrFlight === "DELAY" ? (
                    <div className="col-6 mt-3">
                      <FLInput
                        disable={false}
                        value={phoneEmail}
                        changeEvent={setPhoneEmail}
                        label={"SĐT/Email"}
                        required={true}
                      />
                    </div>
                  ) : null}
                   {typeIsrFlight === "LOST_BAGGAGE" ? (
                    <div className="col-6 mt-3">
                      <FLInput
                        disable={false}
                        value={phoneEmail}
                        changeEvent={setPhoneEmail}
                        label={"SĐT/Email"}
                        required={true}
                      />
                    </div>
                  ) : null}
                  {typeIsrFlight === "BAY_AT" ? (
                    <div className="col-6 mt-3">
                      <FLDate
                        disable={false}
                        value={fromDate}
                        changeEvent={setFromDate}
                        label={"Ngày đi"}
                        required={true}
                      />
                    </div>
                  ) : null}
                  {["BAY_AT", "DELAY", "LOST_BAGGAGE"].includes(typeIsrFlight) ? (
                    <div className="col-6 mt-3">
                      <FLInput
                        disable={false}
                        value={maDC}
                        changeEvent={setMaDC}
                        label={"Mã đặt chỗ"}
                        required={true}
                      />
                    </div>
                  ) : null}
                </>
              ) : (
                <>
                  <div className="col-6 mt-3">
                    <FLInput
                      label="Họ Tên"
                      required={true}
                      value={fullname}
                      changeEvent={setFullname}
                    />
                  </div>
                  <div className="col-6 mt-3">
                    <FLInput
                      label="CMND/ CCCD/ HC"
                      required={true}
                      value={infoKYC}
                      setInfoKYC={setInfoKYC}
                    />
                  </div>
                  <div className="col-12 mt-3">
                    <div className="group-ipnv ">
                      <FLDate
                        disable={false}
                        changeEvent={setFromDate}
                        value={fromDate}
                        label={"Ngày đi"}
                        required={true}
                        hideborder={true}
                      />
                      <div className="ver-line"></div>
                      <div className="kkjs">
                        <FLDate
                          disable={false}
                          changeEvent={setReturnDate}
                          value={returnDate}
                          label={"Ngày về"}
                          required={true}
                          hideborder={true}
                        />
                      </div>
                    </div>
                  </div>
                </>
              )}
            </>
          ) : null}

          <div className="col-6 mt-3">
            <button className="btn-search-kt" disabled={props.loading}>
              {props.loading ? (
                <MetroSpinner size={28} loading={props.loading} />
              ) : (
                <>
                  <span className="icon-search-kt">
                    <i className="fas fa-search"></i>
                  </span>
                  Tìm kiếm
                </>
              )}
            </button>
          </div>
        </div>
      </Form>
      <div className={props.certificate ? "mh-hide-div" : ""}>
        <div className="search-default-frame">
          <img
            src={
              props.isnotfound
                ? "/img/sdk/notfound.svg"
                : "/img/sdk/search-icon-big.svg"
            }
          />
          <p>
            {props.isnotfound
              ? "Không tìm thấy Giấy chứng nhận. Vui lòng nhập tìm kiếm Giấy chứng nhận bằng cách nhập trên thanh tìm kiếm"
              : "Vui lòng tìm kiếm Giấy chứng nhận bằng cách nhập trên thanh tìm kiếm để xem Giấy chứng nhận"}
          </p>
        </div>
      </div>
    </div>
  );
};
export default AdvancedSearch;
