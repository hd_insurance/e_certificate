import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";
import CerResult from "./certificateresult";
import CerQR from "./cerqr";
import CerDocs from "./relativedocs";
import SearchInout from "./searchinput";
import { Row, Col, Button, Nav, Tab, Form } from "react-bootstrap";
import QuickSearch from "./quicksearch";
import AdvancedSearch from "./advancedsearch";

const LeftSide = (props) => {
  const { certificate } = props;

  const [typeSearch, setTypeSearch] = useState("quickSearch");
  const handleCheckbox = (type) => {
    setTypeSearch(type);
  };

  return (
    <div md={4} className="c-left-certificate">
      <div className="cleft-container">
        <div className="gsx-content">
          <Tab.Container defaultActiveKey="quickSearch">
            <Row className="kt-search">
              <Nav variant="pills" className="row kt-search-detail">
                <Nav.Item className="col-6 p-0">
                  <Nav.Link
                    eventKey="quickSearch"
                    onClick={() => handleCheckbox("quickSearch")}
                  >
                    <div className="round">
                      <input
                        type="checkbox"
                        id="checkbox"
                        checked={typeSearch === "quickSearch"}
                        readOnly
                      />
                      <label htmlFor="checkbox"></label>
                      <div>Tìm kiếm nhanh</div>
                    </div>
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item className="col-6 p-0">
                  <Nav.Link
                    eventKey="advancedSearch"
                    onClick={() => handleCheckbox("advancedSearch")}
                  >
                    <div className="round">
                      <input
                        type="checkbox"
                        id="checkbox"
                        checked={typeSearch === "advancedSearch"}
                        readOnly
                      />
                      <label htmlFor="checkbox"></label>
                      <div>Tìm kiếm nâng cao</div>
                    </div>
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Row>
            <div>
              <Tab.Content>
                <Tab.Pane eventKey="quickSearch">
                  <QuickSearch
                    loading={props.loading}
                    search={props.search}
                    certificate={certificate}
                    isnotfound={props.isnotfound}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="advancedSearch">
                  <AdvancedSearch 
                    loading={props.loading}
                    search={props.search}
                    certificate={certificate}
                    isnotfound={props.isnotfound}
                    dataDefine={props.dataDefine}
                  />
                </Tab.Pane>
              </Tab.Content>
            </div>
          </Tab.Container>
      
          <div className={certificate ? "mh-show-div mt-3" : "mh-hide-div mt-3"}>
            <Tab.Container defaultActiveKey="tab1">
              <div className="tab-search-result">
                <Nav variant="pills" className="row">
                  <Nav.Item className="col-4">
                    <Nav.Link eventKey="tab1">Thông tin chung</Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="col-4">
                    <Nav.Link eventKey="tab2">QR giấy chứng nhận</Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="col-4">
                    <Nav.Link eventKey="tab3">Các tài liệu khác</Nav.Link>
                  </Nav.Item>
                </Nav>
              </div>
              <div className="scroll-div">
                <div className="result-expanceds">
                  <Tab.Content>
                    <Tab.Pane eventKey="tab1">
                      <div className="result-hqk-certificate">
                      <CerResult
                        cerDetail={certificate ? certificate : {}}
                      />
                      </div>
                     
                    </Tab.Pane>
                    <Tab.Pane eventKey="tab2">
                      <CerQR certificate={certificate ? certificate : null} />
                    </Tab.Pane>
                    <Tab.Pane eventKey="tab3">
                      <CerDocs files={certificate ? certificate : []} />
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </div>
            </Tab.Container>
          </div>
        </div>

        <div className="footer-xslide">
          <p>
            Liên hệ theo <span className="red-bold"> Hotline 1900068898</span>{" "}
            để được hỗ trợ
          </p>
        </div>
      </div>
    </div>
  );
};
export default LeftSide;
