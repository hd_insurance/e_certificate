import React, { useState, useRef, useEffect } from 'react';
import cogoToast from "cogo-toast";

import Network from "../../services/Network";
const api = new Network();

const Countdown = (props) => {
    const Ref = useRef(null);

    const [timer, setTimer] = useState('00:00');

    const getTimeRemaining = (e) => {
        const total = Date.parse(e) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);
        const hours = Math.floor((total / 1000 * 60 * 60) % 24);
        return {
            total, hours, minutes, seconds
        };
    }



    const startTimer = (e) => {
        let { total, hours, minutes, seconds }
            = getTimeRemaining(e);
        if (total >= 0) {
            setTimer(
                // (hours > 9 ? hours : '0' + hours) + ':' +
                (minutes > 9 ? minutes : '0' + minutes) + ':'
                + (seconds > 9 ? seconds : '0' + seconds)
            )
        }
    }


    const clearTimer = (e) => {
        setTimer('03:00');
        if (Ref.current) clearInterval(Ref.current);
        const id = setInterval(() => {
            startTimer(e);
        }, 1000)
        Ref.current = id;
    }

    const getDeadTime = () => {
        let deadline = new Date();
        deadline.setSeconds( deadline.getSeconds() + 180 );
        return deadline;
    }

    useEffect(() => {
        clearTimer(getDeadTime());
    }, []);

    const onClickReset = (e) => {
        e.preventDefault();
      reSendToken((rs) =>{
          if(rs.Success){
              clearTimer(getDeadTime());
              // props?.billingRequest();
              // billingRequest();
          }else{
              cogoToast.error(rs.ErrorMessage);
          }
      });
    }

    const reSendToken = async (callback) =>{
      let rs = await api.post(`/api/create-token/${props?.cerNo}`);
      callback(rs);
    }

    return (
        <div className="App">
            {timer === '00:00' ? (
                <p className="txt-repeat">Không nhận được email?<label onClick={(e) => onClickReset(e)}>Gửi lại</label></p>
            ): (
                <p className="txt-repeat">Gửi lại mail sau
                    <label>
                        {timer}
                    </label>
                </p>
            )}
        </div>
    )
}

export default Countdown;