import React, { useEffect, useState, createRef } from "react";
import { useScrollPosition } from "@n8tb1t/use-scroll-position";

const ScrollContent = (props) => {
  const [hideOnScroll, setHideOnScroll] = useState(true);
  const mref = createRef();
  var prevscroll = 0;
  const onScroll = () => {
    const scrollY = window.scrollY; //Don't get confused by what's scrolling - It's not the window
    const scrollTop = mref.current.scrollTop;
    if (scrollTop > prevscroll) {
      console.log("show");
    } else {
      console.log("hide");
    }
    prevscroll = mref.current.scrollTop;
  };

  return (
    <div className="doc-scroll" style={{ height: props.height }}>
      {props.children}
    </div>
  );
};
export default ScrollContent;
