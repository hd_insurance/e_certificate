import React, { useEffect, useState, createRef, useRef } from "react";
import { Document, Page, pdfjs } from "../libs/react-pdf/entry.js";
import { motion } from "framer-motion";
import ScrollContent from "./scrollpdf";
import printJS from "print-js";
import QuickSearch from "./quicksearch";
import AdvancedSearch from "./advancedsearch";
import { StageSpinner } from "react-spinners-kit";
import LoadingPrint from "../libs/loading/index.js";

import {
  Row,
  Button,
  OverlayTrigger,
  Nav,
  Tab,
  Tooltip,
} from "react-bootstrap";
import { BrowserView, MobileView } from "react-device-detect";
import Network from "../../services/Network";
const api = new Network();

// pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;

let pdfjsLib = window["pdfjs-dist/build/pdf"];
pdfjs.GlobalWorkerOptions.workerSrc = "/build/pdf.worker.js";

const CertificateContent = (props) => {
  const { certificate } = props;

  const [numPages, setNumPages] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [rendering, setRendering] = useState(false);
  const [zoom, setZoom] = useState(1);
  const [divH, setDivH] = useState(900);
  const [divW, setDivW] = useState(600);
  const [loading, setLoading] = useState(false);
  const [typeSearch, setTypeSearch] = useState("quickSearch");
  const [loadingPrint, setLoadingPrint] = useState(false);
  const [urlCertificate, setUrlCertificate] = useState("");

  const handleCheckbox = (type) => {
    setTypeSearch(type);
  };
  const canvasRef = useRef(null);

  useEffect(() => {
    if (certificate) {
      let url = certificate.URL_CERTIFICATE;
      if (!certificate?.IS_WAIT) {
        setLoading(false);
        setUrlCertificate(url);
        return;
      }
      if (certificate?.IS_WAIT !== "TRUE") {
        setLoading(false);
        setUrlCertificate(url);
        return;
      }
      setLoading(true);
      if (certificate?.IS_WAIT === "TRUE") {
        url = `${url}${certificate.FILE_SIGNED}`;
        setTimeout(() => {
          setUrlCertificate(url);
          setLoading(false);
        }, 4000);
      }
    }
  }, [certificate]);

  // useEffect(() => {
  //   if (certificate) {
  //     let url = certificate.URL_CERTIFICATE;
  //     if (certificate?.IS_WAIT === "TRUE") {
  //       url = `${url}${certificate.FILE_SIGNED}`;
  //     }
  //     // handleGenPdf(url);
  //   }
  // }, [zoom]);

  useEffect(() => {
    setDivH(window.innerHeight - 180);
    if (window.innerWidth < 600) {
      setDivW(window.innerWidth - 60);
    } else {
      setDivW(window.innerWidth - 525);
    }
  }, []);
  function onDocumentLoadSuccess(state) {
    const emptylist = [];
    for (var i = 0; i < state.numPages; i++) {
      emptylist.push("aa");
    }
    setNumPages([...emptylist]);
  }

  // const handleGenPdf = (url) => {
  //   setRendering(true);
  //   // Asynchronous download of PDF
  //   const loadingTask = pdfjsLib.getDocument(url);
  //   loadingTask.promise.then(
  //     function (pdf) {
  //       setNumPages(pdf.numPages);
  //       console.log("PDF loaded");
  //       // Fetch the first page
  //       for (let i = 0; i < pdf.numPages; i++) {
  //         pdf.getPage(i + 1).then(function (page) {
  //           console.log("Page loaded");
  //           var scale = zoom;
  //           var viewport = page.getViewport({ scale: scale });
  //           // Prepare canvas using PDF page dimensions
  //           if (canvasRef.current) {
  //             var context = canvasRef.current.getContext("2d");
  //             canvasRef.current.height = viewport.height;
  //             canvasRef.current.width = viewport.width;
  //             // Render PDF page into canvas context
  //             var renderContext = {
  //               canvasContext: context,
  //               viewport: viewport,
  //             };
  //             var renderTask = page.render(renderContext);
  //             renderTask.promise.then(function () {
  //               console.log("Page rendered");
  //               setRendering(false);
  //             });
  //           }
  //         });
  //       }
  //     },
  //     function (reason) {
  //       // PDF loading error
  //       console.error(reason);
  //     }
  //   );
  // };

  const actionZoomB = () => {
    if (rendering) return;
    const ax = zoom + 0.2;
    setZoom(ax);
  };
  const actionZoomS = () => {
    if (rendering) return;
    const ax = zoom - 0.2;
    setZoom(ax);
  };

  const handlePrintGCN = async () => {
    try {
      setLoadingPrint(true);
      const response = await api.post("/api/print-gcn", {
        fileId: certificate.FILE_SIGNED,
        gcnId: certificate.CERTIFICATE_NO,
      });
      if (response.success && response.data.FILE_BASE64) {
        setLoadingPrint(false);
        printJS({
          printable: response.data.FILE_BASE64,
          type: "pdf",
          base64: true,
        });
      }
    } catch (err) {
      console.log(err);
      setLoadingPrint(false);
    }
  };

  return (
    <div className="cer-content">
      {loadingPrint ? <LoadingPrint isFullScreen={true} /> : null}
      <motion.div
        animate={{
          x: 0,
          y: certificate ? 0 : -63,
          scale: 1,
          rotate: 0,
        }}
      >
        {certificate && (
          <Row className="top-content">
            <div className="col-6">
              <div className="gcn-title1">Giấy chứng nhận</div>
              <div className="gcn-title2">
                <p>
                  <div className="web">Bạn đang xem giấy chứng nhận</div>{" "}
                  <div className="mobile">Bạn đang xem  thể hiện GCN</div>{" "}
                  <span>
                    {certificate ? certificate.CERTIFICATE_NO : "Chưa có"}
                  </span>
                </p>
              </div>
            </div>

            <div className="col-6 x3ax">
              <div className="content-action-group">
                <OverlayTrigger
                  placement="bottom"
                  overlay={
                    <Tooltip id="tooltip-disabled">Gửi mail GCN</Tooltip>
                  }
                >
                  <div className="circle-button email">
                    <i className="fas fa-envelope"></i>
                    <p>Gửi mail GCN</p>
                  </div>
                </OverlayTrigger>
                <OverlayTrigger
                  placement="bottom"
                  overlay={<Tooltip id="tooltip-disabled">Tải GCN</Tooltip>}
                >
                  <a
                    href={`${certificate.URL_CERTIFICATE}?download=true`}
                    className="circle-button"
                  >
                    <i className="fas fa-file-download download"></i>
                    <p>Tải GCN</p>
                  </a>
                </OverlayTrigger>
                <OverlayTrigger
                  placement="bottom"
                  overlay={<Tooltip id="tooltip-disabled">In GCN</Tooltip>}
                >
                  <div className="circle-button print" onClick={handlePrintGCN}>
                    <i className="fas fa-print"></i>
                  </div>
                </OverlayTrigger>
                <Button className="view-render-gcn">Xem thể hiện GCN</Button>
              </div>
            </div>
          </Row>
        )}
      </motion.div>

      {certificate ? (
        <div className="doc-con" style={{ height: divH }}>
          {loading ? (
            <div className="style_loading_cert">
              <StageSpinner size={50} loading={loading} color={"#329945"} />
            </div>
          ) : (
            <ScrollContent height={divH}>
              <div className="document-content-container">
                {/* <canvas ref={canvasRef} id="cert-canvas"></canvas> */}
                <Document
                  file={urlCertificate}
                  loading={""}
                  onLoadSuccess={onDocumentLoadSuccess}
                >
                  {numPages.map((page, index) => {
                    return (
                      <div className="pp-page" key={index}>
                        <Page
                          loading={""}
                          renderMode={"canvas"}
                          renderTextLayer={false}
                          scale={zoom}
                          pageNumber={index + 1}
                          width={divW}
                        />
                      </div>
                    );
                  })}
                </Document>
              </div>
            </ScrollContent>
          )}
          {/* <div className="page-name-counter">
            Trang {pageNumber}/{numPages}
          </div> */}
          <div className="pdf-btn-group">
            <div className="pdf-btn" onClick={(e) => actionZoomB()}>
              <i className="fas fa-plus"></i>
            </div>
            <div className="pdf-btn" onClick={(e) => actionZoomS()}>
              <i className="fas fa-minus"></i>
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-docu">
          <BrowserView>
            <div className="empty-docu-container">
              <img src="/img/sdk/document.svg" />
              <p>Không có giấy chứng nhận nào để hiển thị.</p>
              <p>Vui lòng tìm giấy chứng nhận</p>
            </div>
          </BrowserView>
          <MobileView>
            <div className="search-mobile-v">
              <Tab.Container defaultActiveKey="quickSearch">
                <Row className="kt-search">
                  <Nav variant="pills" className="row kt-search-detail">
                    <Nav.Item className="col-6 p-0">
                      <Nav.Link
                        eventKey="quickSearch"
                        onClick={() => handleCheckbox("quickSearch")}
                      >
                        <div className="round">
                          <input
                            type="checkbox"
                            id="checkbox"
                            checked={typeSearch === "quickSearch"}
                          />
                          <label for="checkbox"></label>
                          <div>Tìm kiếm nhanh</div>
                        </div>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="col-6 p-0">
                      <Nav.Link
                        eventKey="advancedSearch"
                        onClick={() => handleCheckbox("advancedSearch")}
                      >
                        <div className="round">
                          <input
                            type="checkbox"
                            id="checkbox"
                            checked={typeSearch === "advancedSearch"}
                          />
                          <label for="checkbox"></label>
                          <div>Tìm kiếm nâng cao</div>
                        </div>
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Row>
                <div>
                  <Tab.Content>
                    <Tab.Pane eventKey="quickSearch">
                      <QuickSearch
                        loading={props.loading}
                        search={props.search}
                        certificate={certificate}
                        isnotfound={props.isnotfound}
                      />
                    </Tab.Pane>
                    <Tab.Pane eventKey="advancedSearch">
                      <AdvancedSearch
                        loading={props.loading}
                        search={props.search}
                        certificate={certificate}
                        isnotfound={props.isnotfound}
                        dataDefine={props.dataDefine}
                      />
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </Tab.Container>
            </div>
          </MobileView>
        </div>
      )}
    </div>
  );
};
export default CertificateContent;
