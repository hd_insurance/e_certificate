import React, { useEffect, useState } from "react";
import { Col, Button, Form } from "react-bootstrap";
import FLInput from "../libs/uikit/flinput";
import { MetroSpinner } from "react-spinners-kit";

const QuickSearch = (props) => {
  const { url } = props;
  const [validated, setValidated] = useState(false);
  const [value, setValue] = useState("");
  const [gcn, setGCN] = useState("");

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() === false) {
      return setValidated(true);
    }
    let data = {
      a1: gcn.trim(),
      a2: value.trim(),
      a3: "GENERAL",
    };
    props.search(data);
  };
  // useEffect(() => {
  //   let data = {
  //     a1: "S21CNERINMX",
  //     a2: "Nguyễn Thị Ngọc Trân",
  //     a3: "GENERAL",
  //   };
  //   props.search(data);
  // }, []);

  return (
    <div>
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <div className="row mt-3">
          <div className="col-12">
            <FLInput
              label="Email/ Họ Tên/ Số điện thoại/ CMND/ CCCD/ HC"
              required={true}
              value={value}
              changeEvent={setValue}
            />
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-6">
            <FLInput
              label="Số giấy chứng nhận"
              value={gcn}
              changeEvent={setGCN}
              required={true}
            />
          </div>
          <div className="col-6">
            <button className="btn-search-kt" disabled={props.loading}>
              {props.loading ? (
                <MetroSpinner size={28} loading={props.loading} />
              ) : (
                <>
                  <span className="icon-search-kt">
                    <i className="fas fa-search"></i>
                  </span>
                  Tìm kiếm
                </>
              )}
            </button>
          </div>
        </div>
      </Form>
      <div className={props.certificate ? "mh-hide-div" : ""}>
        <div className="search-default-frame">
          <img
            src={
              props.isnotfound
                ? "/img/sdk/notfound.svg"
                : "/img/sdk/search-icon-big.svg"
            }
          />
          <p>
            {props.isnotfound
              ? "Không tìm thấy Giấy chứng nhận. Vui lòng thử lại bằng tìm kiếm nâng cao"
              : "Vui lòng tìm kiếm Giấy chứng nhận bằng cách nhập trên thanh tìm kiếm để xem Giấy chứng nhận"}
          </p>
        </div>
      </div>
    </div>
  );
};
export default QuickSearch;
