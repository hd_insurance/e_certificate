
import React, {useState, createRef, useRef} from "react";
import {Button,Form, Row, Modal} from "react-bootstrap";
import Language from "../../services/Language.js";
import FLInput from "../libs/uikit/flinput";
import cogoToast from "cogo-toast";
import { MetroSpinner } from "react-spinners-kit";

import Countdown from "./Countdown";

import Network from "../../services/Network";
const api = new Network();

const l = new Language();

const CertificateResult = (props) => {
  const { cerDetail } = props;
  const formRef = createRef();
  const formRef1 = createRef();
  const [validated, setValidated] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [codeConfirm, setCodeConfirm] = useState(null);
  const [showSuccess, setShowSuccess] = useState(false);
  const [message, setMessage] = useState(null);
  const [msgSuccess, setMsgSuccess] = useState(null);
  const [showTime, setShowTime] = useState(true);

  const [mode, setMode] = useState("0");
  const [name, setName] = useState(null);
  const [mst, setMst] = useState(null);
  const [email, setEmail] = useState(null);
  const [address, setAddress] = useState(null);

  const [loadingBtn, setLoadingBtn] = useState(false);

  const onRdChange = (e) => {
    if (e.target.checked) {
      setMode(e.target.value);
    }
  };

  const showModalBillingRequest = () =>{
    setName(null);
    setMst(null);
    setEmail(null);
    setAddress(null);
    setCodeConfirm(null);
    setValidated(false);
    setShowModal(true);
  }

  const submitForm = () =>{
    const form = formRef.current;
    setLoadingBtn(true);
    if (form.checkValidity() === false) {
      setValidated(true);
      setLoadingBtn(false);
      return false;
    }
    createToken();
  }

  const submitFormConfirm = (e) =>{
    e.preventDefault();
    const form = formRef1.current;
    setLoadingBtn(true);
    if (form.checkValidity() === false) {
      setValidated(true);
      setLoadingBtn(false);
      return false;
    }
    confirmToken();
  }

  const billingRequest = async () =>{
    setLoadingBtn(true);
    const param = {
      detail_code:  cerDetail.DETAIL_CODE,
      taxcode: mode === "1" ?  mst : null,
      email: email,
      address:  address,
      e_name:  name,
      type: mode === "0" ? 'CN' : 'CQ',
    };
    try{
      const response = await api.post(`/api/billing-request/${cerDetail?.ORG_CODE}`, param);
      if(response.success){
        setLoadingBtn(false);
        // setShowModal(false);
        // setShowSuccess(true);
      }else{
        setLoadingBtn(false);
        cogoToast.error(response.error_message);
      }
    }catch (e){
      cogoToast.error(e.error_message)
    }
  }

  const createToken = async () =>{
    setLoadingBtn(true);
    try{
      const rs = await api.post(`/api/create-token/${cerDetail?.CERTIFICATE_NO}`);
      if(rs.Success){
        setLoadingBtn(false);
        setShowConfirm(true);
        setMessage(rs?.Data?.Message);
      }else{
        setLoadingBtn(false);
        cogoToast.error(rs.ErrorMessage);
      }
    }catch (e){
      setLoadingBtn(false);
      cogoToast.error(e.ErrorMessage);
    }
  }
  const confirmToken = async () =>{
    try{
      const rs = await api.post(`/api/confirm-token/${cerDetail?.CERTIFICATE_NO}/${codeConfirm}`);
      if(rs.Success){
        setLoadingBtn(false)
        // cogoToast.success(rs?.Data?.Message)
        billingRequest();
        setMsgSuccess(rs?.Data?.Message);
        setShowConfirm(!showConfirm);
        setShowSuccess(true);
      }else{
        setLoadingBtn(false);
        setShowSuccess(false);
        cogoToast.error(rs.ErrorMessage);
      }
    }catch (e){
      setLoadingBtn(false);
      setShowSuccess(false);
      cogoToast.error(e.ErrorMessage);
    }
  }

  const closeModal =() =>{
    setShowConfirm(false);
    setShowModal(false);
    setShowSuccess(false);
  }


  return (
    <>
      <div className="certificate-result">
        <div className="ahewader">
          <div className="hdi-icon">
            <img src="/img/sdk/hdi-usr.svg" />
          </div>
          <div className="sjtitle">
            <span className="gcn-num">Số GCN:</span>
            <span className="gcn-id">{cerDetail.CERTIFICATE_NO}</span>
          </div>
        </div>

        <div className="result-body">
          <div className="rs-row">
            <span className="name">Loại Bảo Hiểm:</span>
            <span className="value">
              {cerDetail.PRODUCT_NAME || "Không có"}
            </span>
          </div>
          <div className="rs-row">
            <span className="name">Tên người được bảo hiểm:</span>
            <span className="value">{cerDetail.NAME || "Không có"}</span>
          </div>
          <div className="rs-row">
            <span className="name">Thời hạn bảo hiểm:</span>
            <span className="value">
              {cerDetail.EFFECTIVE_DATE} - {cerDetail.EXPIRATION_DATE}
            </span>
          </div>
          {cerDetail?.infoExt?.length
            ? cerDetail.infoExt.map((info, index) => {
                return (
                  <div className="rs-row" key={index}>
                    <span className="name">{info.DESCRIPTION}:</span>
                    <span className="value">{info.VALUE}</span>
                  </div>
                );
              })
            : null}
          {cerDetail?.IS_BILL === "1" && (
              <div className="btn-bill-request" onClick={() => showModalBillingRequest()}>
                Chỉnh sửa thông tin xuất hoá đơn
              </div>
          )}
          <Modal
              show={showModal}
              onHide={closeModal}
              backdrop="static"
              keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>Chỉnh sửa thông tin xuất hoá đơn</Modal.Title>
            </Modal.Header>
            <Modal.Body style={{fontSize: 14}}>
              {!showConfirm && !showSuccess ? (
                  <>
                    <label>Vui lòng điền đầy đủ các thông tin sau để Bảo Hiểm HD xuất hoá đơn</label>
                    <Form
                        ref={formRef}
                        className="search-form-input"
                        noValidate
                        validated={validated}
                    >
                      <div  style={{display: "flex"}}>
                        <label>Bạn muốn xuất hoá đơn cho?</label>

                        <div className="rq_rd_group" style={{marginLeft: 24}}>
                          <input
                              className="radio"
                              type="radio"
                              name="rd1"
                              value={"0"}
                              onChange={(e) => onRdChange(e)}
                              id="radio-1"
                              defaultChecked={mode === "0"}
                          />
                          <label htmlFor="radio-1" style={{fontSize: 14}}>Cá nhân</label>
                          <div className="sp-hoz"/>
                          <input
                              className="radio"
                              type="radio"
                              name="rd1"
                              value={1}
                              onChange={(e) => onRdChange(e)}
                              id="radio-2"
                              defaultChecked={mode === "1"}
                              style={{fontSize: 14}}
                          />
                          <label htmlFor="radio-2" style={{fontSize: 14}}>Doanh nghiệp</label>
                        </div>
                      </div>
                      <div className={"col-12 form-group"}>
                        <FLInput
                            disable={false}
                            value={name}
                            changeEvent={setName}
                            label="Họ và tên"
                            required={true}
                        />
                      </div>
                      {mode === "1" &&(
                          <div className={"col-12 form-group"}>
                            <FLInput
                                disable={false}
                                value={mst}
                                changeEvent={setMst}
                                label="Mã số thuế doanh nghiệp"
                                required={true}
                            />
                          </div>
                      )}
                      <div className={"col-12 form-group"}>
                        <FLInput
                            disable={false}
                            value={email}
                            changeEvent={setEmail}
                            label="Email nhận hoá đơn"
                            required={true}
                        />
                      </div>
                      <div className={"col-12 form-group"}>
                        <FLInput
                            disable={false}
                            value={address}
                            changeEvent={setAddress}
                            label="Địa chỉ xuất hoá đơn"
                            required={true}
                        />
                      </div>
                    </Form>
                    <div className={"footer-modal-request"}>


                      <button className="btn-request-invoice"
                           style={{margin: 'auto'}}
                           type="submit"
                           onClick={() =>submitForm()}
                           disabled={loadingBtn}
                      >
                        {loadingBtn ? (
                            <MetroSpinner size={28} loading={props.loading} />
                        ) : (
                            'Lưu chỉnh sửa hoá đơn'
                          )}
                      </button>
                      <div style={{marginTop: 16}}>Mọi thông tin thắc mắc xin liên hệ theo hotline:  <label className="request-hotline">1900 068898</label></div>
                    </div>
                  </>
              ) : showConfirm && !showSuccess ? (
                  <div className="dv-confirm">
                    <img src={"/img/ic-email-confirm.svg"} style={{marginBottom: 16}} />
                    <div>{message}</div>
                    <p>Vui lòng nhập mã xác nhân để hoàn tất yêu cầu chỉnh sửa.</p>
                    <Form
                        ref={formRef1}
                        className="search-form-input"
                        noValidate
                        validated={validated}>
                      <div className={"col-12 form-group"} style={{textAlign: 'left'}}>
                        <FLInput
                            disable={false}
                            value={codeConfirm}
                            changeEvent={setCodeConfirm}
                            label="Mã xác thực"
                            required={true}
                        />
                      </div>
                      <button className="btn-request-invoice"
                           style={{margin: 'auto'}}
                           type="submit"
                           onClick={(e) =>submitFormConfirm(e)}
                           disabled={loadingBtn}
                      >
                        {loadingBtn ? (
                                <MetroSpinner size={28} loading={props.loading} />
                            ): (
                                'Xác thực'
                        )}
                      </button>

                      <Countdown
                          total={3}
                          billingRequest={billingRequest}
                          cerNo={cerDetail?.CERTIFICATE_NO}
                      />
                    </Form>
                  </div>
              ) : (
                  <div className='form_confirm_success'>
                    <img src='/img/ic_success.png' width={80} height={80}/>
                    <p className='txt_title_success'>Chỉnh sửa thành công</p>
                    <p className='txt_content_success'>
                      Hoá đơn VAT của bạn sẽ được Bảo Hiểm HD gửi đến email: <span>{email}</span>. Xin cám ơn!
                    </p>
                    <div className='close_form_success' onClick={closeModal}>
                      Đóng
                    </div>
                  </div>

              )}

            </Modal.Body>
          </Modal>
        </div>
      </div>


      <div className="rs-menu dlhide xxx">
        <Row>
          <div className="col-3">
            <div className="menu-btn">
              <div className="circle-btn bg1">
                <i className="fas fa-file-signature"></i>
              </div>
              <p>Yêu cầu tái tục</p>
            </div>
          </div>
          <div className="col-3">
            <div className="menu-btn">
              <div className="circle-btn bg2">
                <i className="fas fa-plus-circle"></i>
              </div>
              <p>YC bồi thường</p>
            </div>
          </div>
          <div className="col-3">
            <div className="menu-btn">
              <div className="circle-btn bg2">
                <i className="fas fa-edit"></i>
              </div>
              <p>Sửa đổi BS</p>
            </div>
          </div>
          <div className="col-3">
            <div className="menu-btn">
              <div className="circle-btn bg3">
                <i className="fas fa-times-circle"></i>
              </div>
              <p>Hủy HĐ</p>
            </div>
          </div>
        </Row>
      </div>
    </>
  );
};
export default CertificateResult;
