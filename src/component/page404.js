import React, { useEffect, useState } from "react";

const Page404 = (props) => {
  return (
    <div className="pg-container">
      <div className="bnn">
        <div className="img-bg-xbg-dfpage bnwarning">
          <div className="row bnn-content-fullw">
            <div className="col-md-5 img-warning mobile">
              <img src="./public/img/warning-img.svg" />
            </div>

            <div className="col-md-7">
              <div className="bnn-content btn_red ">
                <h1>Page not found</h1>
                <p>
                  Không tìm thấy trang bạn muốn, liên hệ quản trị viên để được
                  trợ giúp
                </p>
                <a href="/">
                  Quay lại <i className="fas fa-chevron-right"></i>
                </a>
              </div>
            </div>
            <div className="col-md-5 img-warning web">
              <img src="/img/warning-img.svg" />
            </div>
          </div>
        </div>
      </div>

      <div className="sv-content">
        <div className="partice-bg"></div>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="sv-item">
                <div className="t-thumb">
                  <img src="/img/dfpage/v0.png" />
                </div>
                <h5>Liên hệ hỗ trợ</h5>
                <p>
                  Hãy gọi theo số điện thoại{" "}
                  <span className="text-green bbold">1900 068 898</span> để được
                  hỗ trợ 1 cách chu đáo và nhiệt tình từ đội ngũ CSKH của Bảo
                  hiểm HD
                </p>
                <a className="green-btn-dx" href="#">
                  <i className="fas fa-phone"></i> Liên hệ
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="sv-item">
                <div className="t-thumb">
                  <img src="/img/dfpage/v1.png" />
                </div>
                <h5>Để lại lời nhắn</h5>
                <p>
                  Gửi lại lời nhắn cho chúng tôi thông qua mail{" "}
                  <span className="text-green bbold">
                    product@hdinsurance.com.vn
                  </span>{" "}
                  . Chúng tôi sẽ trả lời lại sớm nhất cho bạn
                </p>
                <a className="green-btn-dx" href="#">
                  <i className="fas fa-envelope"></i> Gửi mail
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="sv-item">
                <div className="t-thumb">
                  <img src="/img/dfpage/v2.png" />
                </div>
                <h5>Khám phá Bảo Hiểm</h5>
                <p>
                  Truy cập vào trang web của Bảo Hiểm HD để khám phá muôn vàn
                  gói bảo hiểm dành cho bạn
                </p>
                <a className="green-btn-dx" href="https://hdinsurance.com.vn">
                  <i className="fas fa-globe"></i> Truy cập
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Page404;
